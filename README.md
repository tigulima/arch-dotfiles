# Lemony Dotfiles
## My lovingly built custom configuration files

![alt text](https://thiagoimai.com/assets/distro.png "Distro")

Lemony is a customized version of EndeavourOS BSPWM comunity edition (Arch)

## Apps

- Better Discord
- BSPWM
- Polybar
- Rofi
- Spicetify
- Thunar

## Installation

It really should be as simple as...

```sh
git clone https://gitlab.com/tigulima/arch-dotfiles.git
```

## Social

Feel free to tell me your ideas on how to enhance this or any other project.

| Plugin | README |
| ------ | ------ |
| Instagram | https://www.instagram.com/tigulima.exe |
| Twitter | https://twitter.com/Tigulima|
| GitHub | https://github.com/tigulima|
| Reddit | https://www.reddit.com/user/tigulima|

## Author

Thiago Imai Lima

> Free Software, Hell Yeah!
